#pragma semicolon 1
#include <clientprefs>
#define HIDE_RADAR_CSGO 1<<12
#pragma newdecls required
public Plugin myinfo =
{
	name = "Disable HUD radar/money deluxe",
	author = "Pheonix (˙·٠●Феникс●٠·˙) & ShaRen",
	version = "1.1",
	url = "zizt.ru Servers-Info.Ru"
};



Handle mp_maxmoney;
bool g_bU[MAXPLAYERS+1];
bool g_bIsMenu[MAXPLAYERS+1];
char g_cM[10];

public void OnPluginStart() {
	mp_maxmoney = FindConVar("mp_maxmoney");
	GetConVarString(mp_maxmoney, g_cM, 10);
	CreateTimer(0.5, Timer_CheckMenu, _, TIMER_REPEAT);
}

public Action Timer_CheckMenu(Handle timer)
{
	for (int i=1; i<=MaxClients; i++)
		if (IsClientInGame(i))
			if (!g_bU[i] && GetClientMenu(i) != MenuSource_None) {
				if (!g_bIsMenu[i]) {
					g_bIsMenu[i] = true;
					HidePanels(i, true);
				}
			} else if (g_bIsMenu[i]) {
				g_bIsMenu[i] = false;
				HidePanels(i, false);
			}
}

void HidePanels(int iClient, bool bHide)
{
	if (bHide) {
		SendConVarValue(iClient, mp_maxmoney, "0");
		if (!IsPlayerAlive(iClient))
			SetEntProp(iClient, Prop_Send, "m_iHideHUD", GetEntProp(iClient, Prop_Send, "m_iHideHUD") | HIDE_RADAR_CSGO);
	} else {
		SendConVarValue(iClient, mp_maxmoney, g_cM);
		SetEntProp(iClient, Prop_Send, "m_iHideHUD", GetEntProp(iClient, Prop_Send, "m_iHideHUD") & ~HIDE_RADAR_CSGO);
	}
}

// может возникать ошибка если использовать bot_mimic 1 и зажимать ТАБ
public Action OnPlayerRunCmd(int iClient, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon) {
	if (buttons & IN_SCORE) {
		if(!g_bU[iClient]) {
			g_bU[iClient] = true;
			HidePanels(iClient, false);
		}
	} else if(g_bU[iClient]) {
		g_bU[iClient] = false;
		if (GetClientMenu(iClient))
			HidePanels(iClient, true);
	}
	return Plugin_Continue;
}
